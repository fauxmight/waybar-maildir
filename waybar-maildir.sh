#!/usr/bin/env bash
#
# waybar-maildir.sh - count messages in a maildr (or other) directory
#
# USAGE: waybar-maildir.sh DIRECTORY ACCOUNT_NAME [nonotify]
#        "nonotify" is used to prevent multiple identical notification when this script is used in multi-monitor setups
#
# REQUIREMENTS: inotifywait
#               (optional) mu - to collect full 'Subject' line
#
#
#
### Example (partial) ~/.config/waybar/config
#
# "custom/maildir": {
#   "exec": "~/.config/waybar/waybar-maildir.sh ~/.maildir/INBOX/new mailaccountname",
#   "format": " {}",
#   "return-type": "json"
# },
#
### Example (partial) ~/.config/waybar/syles.css
#
# #custom-maildir {
#     background-color: #875f00
# }

# mail_dir         is the directory we are watching and within which, we are counting contents
# account_name     can be used in notify_command as a label for the mail account being watched
# notify_icon_path is the path to an icon for use in notify-send ... in case you use notify-send in $notify_command
# notify_command   is a command to run when new mail exists (perhaps notify-send ...)

mail_dir="${1}"
account_name="${2}"
notify_icon_path="/usr/share/icons/Adwaita/scalable/status/mail-unread-symbolic.svg"
notify_command='notify-send -i $notify_icon_path "${count} NEW messages in \"${account_name}\"" "Latest: $subject\nFrom $from"'

old_count=0

while [[ -n "$(pgrep waybar)" ]]; do
    subject="SUBJECT UNAVAILABLE"
    if [[ ! -d "${mail_dir}" ]]; then
	printf -- '{"text":"?"}\n'
	sleep 60
    fi

    count="$(ls "${mail_dir}" | wc -l)"

    if [[ "${count}" > 0 ]]; then
	printf -- '{"text":"%s","class":"newmail","tooltip":"%s"}\n' "${count}" "${account_name}"
    else
	unset latest_message_file
	printf -- '{"text":"-","tooltip":"%s"}\n' "${account_name}"
    fi
    
    if [[ ( "${3}" != "nonotify" ) && ( "${count}" > "${old_count}" ) ]]; then
	latest_message_file="${mail_dir}"/$(ls -t "${mail_dir}" | head -n1)
	# If `mu` is availalbe, use it to get WHOLE subject line
	subject=$(mu view "${latest_message_file}" | grep '^Subject: ' | sed -e 's/^Subject: //') ||
	    subject=$(grep '^Subject: ' "${latest_message_file}"       | sed -e 's/^Subject: //')
	from=$(mu view "${latest_message_file}"    | grep '^From: '    | sed -e 's/^From: //') ||
	    from=$(grep '^FROM: ' "${latest_message_file}"             | sed -e 's/^From: //')
	eval "${notify_command}"
    fi
    
    old_count="${count}"

    # 60-second timeout keeps this from becoming a zombified process when waybar is no longer running
    inotifywait -t 60 -qq -e 'create,delete,move' "${mail_dir}"
    sleep 1 # Allow directory to settle after new messages

done

unset -v account_name count latest_file mail_dir nonotify notify_command old_count subject
